
public class ObservateurCoordonnee implements Observateur {
    private String modification = "PAS DE MODIFICATION";
    private String nom;

    @Override
    public void metsAJour(String attributModifie, Object valeur) {
        modification = "Attribut : " + attributModifie + " Valeur : " + valeur;
    }

    public String getModification() {
        return modification;
    }
    
    public ObservateurCoordonnee()
    {
      this.nom = "";
    }

    public ObservateurCoordonnee(String n)
    {
      this.nom = n;
    }

    public String getNom()
    {
      return this.nom;
    }
    

}
